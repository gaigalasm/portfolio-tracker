import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import React from "react"
import StockPage from './components/StockPage/StockPage';
import PortfolioPage from './components/PortfolioPage/PortfolioPage';
import ErrorPage from './components/Error/Error'
import LoginPage from './components/Login/Login'
import useToken from './useToken'

function App() {
  const {token, setToken} = useToken()

  if(!token) {
    return <LoginPage setToken={setToken} />
  }

  return (
      <Router>
        <div>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link className="nav-link" to="/">StockPage</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/portfolio">Portfolio</Link>
              </li>
            </ul>
          </nav>

          <Switch>
            <Route path="/" component={StockPage} exact />
            <Route path="/error" component={ErrorPage} />
            <Route path="/login" component={LoginPage} />
            <Route path="/portfolio" component={PortfolioPage} />
          </Switch>
        </div>
      </Router>
  );
}

export default App;
