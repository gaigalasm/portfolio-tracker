import { useState } from 'react'

export default function useToken() {
    const getToken = () => {
        const token = sessionStorage.getItem('token')
        const toketObject = JSON.parse(token)
        return toketObject?.token
    }

    const [token, setToken] = useState(getToken())

    const saveToken = userToken => {
        sessionStorage.setItem('token', JSON.stringify(userToken))
        setToken(useToken.token)
    }

    return {
        setToken : saveToken,
        token
    }
}