import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders login form", () => {
  render(<App />);
  const username = screen.getByText(/Username/i);
  const password = screen.getByText(/Password/i);
  expect(username).toBeInTheDocument();
  expect(password).toBeInTheDocument();
});
