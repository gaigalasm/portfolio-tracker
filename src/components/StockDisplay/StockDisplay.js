import React from 'react'
import StockAdd from '../StockAdd/StockAdd'

export default function StockDisplay(props) {
    const {stockData} = props

    return(
        <div>
            {stockData && 
                <div>
                    <h1>{stockData.symbol}</h1>
                    <p>Price : {stockData.price}</p>
                    <p>Change : {stockData.change}</p>
                    <p>Change in percentage : {stockData.percentChange}</p>
                    <p>Volume : {stockData.volume}</p>
                    <StockAdd symbol={stockData.symbol} price={stockData.price}/>
                </div>
            }
        </div>
    )
}
