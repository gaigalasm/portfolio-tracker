import Enzyme, { shallow, render, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import StockPage from './StockPage'

Enzyme.configure({ adapter: new Adapter() })

it('renders correctly StockPage spanshot enzyme', () => {
    const wrapper = shallow(<StockPage />)
  
    expect(toJson(wrapper)).toMatchSnapshot();
  });