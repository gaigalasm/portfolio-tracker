import React from 'react'
import StockSearch from '../StockSearch/StockSearch'
import StockDisplay from '../StockDisplay/StockDisplay'

export default class StockPage extends React.Component {
    state = {
        stockData : null
    }

    componentDidUpdate = () => {
        const { stockData } = this.state

        if (stockData) {
            return
        }

        this.getStockData()
    }

    getStockData = (data) => {
        if (data !== this.state.stockData)
            this.setState({stockData: data})
    }
    
    render(){
        const { stockData } = this.state

        return (
            <div className="container">
                <h1>Stock search</h1>
                <p>Enter the stock ticker (like MSFT, IBM, AAPL etc.)</p>
                <StockSearch sendStockData={this.getStockData}/>
                <StockDisplay stockData={stockData}/>
            </div>
        )
    }
}
