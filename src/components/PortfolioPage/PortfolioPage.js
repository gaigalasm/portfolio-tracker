import React from 'react'
import PortfolioItem from '../PortfolioItem/PortfolioItem'

export default class PortfolioPage extends React.Component {
    state = {
        stocks: []
    }

    componentDidMount = async () => {
        await fetch("https://ptracker-backend.herokuapp.com/stocks")
            .then(res => res.json())
            .then(
                (data) => {
                    this.setState({
                        stocks: data
                    });
                }
            ).catch(err => console.log(err))
    }

    renderPortfolioItems = () => {
        const { stocks } = this.state;
        return stocks.map(stock => (
            <PortfolioItem key={stock._id} id={stock._id} symbol={stock.symbol} quantity={stock.quantity} purchasePrice={stock.purchasePrice} latestPrice={stock.latestPrice} change={stock.change} portfolioPercentage={stock.portfolioPercentage} />
        ))
    }


    render(){
        return (
            <div className="container">
                <h1>Porfolio page</h1>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Stock</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Purchase price</th>
                            <th scope="col">Price now</th>
                            <th scope="col">% change of position</th>
                            <th scope="col">% of whole portfolio (WIP)</th>
                            <th scope="col">Update (WIP)</th>
                            <th scope="col">Delete</th>
                        </tr>  
                    </thead>
                    <tbody>
                        {this.renderPortfolioItems()}
                    </tbody>
                </table>
            </div>
        )
    }
    
}
