import React from 'react'

export default class StockSearch extends React.Component {
    state = {
        ticker: ''
    }

    handleChange = (event) => {
        this.setState({ ticker: event.target.value });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const { ticker } = this.state
        const apiKey = "6X9KL5WB1XLAFFHK"
        let stockData

        try{
            await fetch(`https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${ticker.toUpperCase()}&apikey=${apiKey}`)
                .then(response => response.json())
                .then(
                    (data) => {
                        stockData = {
                            symbol: data['Global Quote']['01. symbol'],
                            price: data['Global Quote']['05. price'],
                            volume: data['Global Quote']['06. volume'],
                            change: data['Global Quote']['09. change'],
                            percentChange: data['Global Quote']['10. change percent']
                        }
                    }
                )
        } catch(e){
            console.log(e)
        }

        this.props.sendStockData(stockData);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="text" value={this.state.ticker} onChange={this.handleChange} />
                <input type="submit" value="Search" />
            </form>
        );
    }
}