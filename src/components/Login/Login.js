import React, { useState } from 'react';
import PropTypes from 'prop-types';

async function loginUser(credentials) {
 return fetch('https://ptracker-backend.herokuapp.com/login', {
   method: 'POST',
   headers: {
     'Content-Type': 'application/json'
   },
   body: JSON.stringify(credentials)
 })
   .then(data => {
    if (data.status === 200){
      return data.json()
    } else {
      return null
    }
  })
}

export default function Login({ setToken }) {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [isError, setIsError] = useState(false);

  const handleSubmit = async e => {
    e.preventDefault();
    
    const body = {
      username: username,
      password: password
    }
    
    const token = await loginUser(body);

    if(token === null){
      setIsError(true)
      return
    }

    setToken(token);

    window.location.reload();
  }

  return(
    <div className="container pt-5">
      <h1>Please Log In</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>
            <p>Username</p>
            <input type="text" onChange={e => setUserName(e.target.value)} />
          </label>
        </div>
        <div className="form-group">
          <label>
            <p>Password</p>
            <input type="password" onChange={e => setPassword(e.target.value)} />
          </label>
        </div>
        {isError && <p className="text-danger">Wrong password or username</p>}
        <div>
          <button className="btn btn-light" type="submit">Submit</button>
        </div>
      </form>
    </div>
  )
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
};