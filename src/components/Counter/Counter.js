import React, { useReducer } from "react";

const Counter = () => {
  const [counter, dispatch] = useReducer(
    (counter, addAmount) => counter + addAmount,
    0
  );

  return (
    <div>
      <p data-testid="counter-value">Count: {counter}</p>
      <button onClick={() => dispatch(-2)}>-2</button>
      <button onClick={() => dispatch(-1)}>-1</button>
      <button onClick={() => dispatch(1)}>+1</button>
      <button onClick={() => dispatch(2)}>+2</button>
    </div>
  );
};

export default Counter;
