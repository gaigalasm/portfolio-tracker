import React, { Component } from 'react'

export default class StockAdd extends Component {
    state = {
        buyPrice: null
    }

    handleChange = (event) => {
        this.setState({ buyPrice: event.target.value });
    }

    handleSubmit = async (event) => {
        event.preventDefault()
        const {buyPrice} = this.state
        const {symbol, price} = this.props

        let quantity = (parseInt(buyPrice)/price).toFixed(4)

        const data = {
            symbol: symbol,
            quantity,
            purchasePrice: buyPrice,
            latestPrice: price,
            change: 0,
            portfolioPercentage: 0
        };
        
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        };

        await fetch('https://ptracker-backend.herokuapp.com/addstock', requestOptions)
            .then(response => response.json());

        window.location.reload();
    }
    
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Add stock to the portfolio (fill the price):
                        <input type="text" pattern="[0-9]*" value={this.state.value} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Submit" />
            </form>
        )
    }
}
