function PortfolioItem(props){

    let calculatePriceNow = () => {
        const {quantity, latestPrice} = props

        return (quantity * latestPrice).toFixed(2)
    }

    let calculateChange = () => {
        const {purchasePrice} = props

        return (((calculatePriceNow() - purchasePrice)/purchasePrice) * 100).toFixed(2)
    }

    let handleDelete = async (event) => {
        event.preventDefault()

        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
        };

        await fetch(`https://ptracker-backend.herokuapp.com/stocks/delete/${props.id}`, requestOptions)

        window.location.reload();
    }

    return (
        <tr>
            <th key={props.id} scope="row">{props.symbol}</th>
            <td>{props.quantity}</td>
            <td>{props.purchasePrice}</td>
            <td>{calculatePriceNow()}</td>
            <td data-testid="change-value">{calculateChange()}</td>
            <td>WIP (% or portfolio)</td>
            <td>
                <button className="btn btn-success">Update price (WIP)</button>
            </td>
            <td>
                <button className="btn btn-danger" onClick={handleDelete}>Delete</button>
            </td>
        </tr>);
}

export default PortfolioItem