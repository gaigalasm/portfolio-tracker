import { fireEvent, render } from "@testing-library/react";
import React from "react";

import PortfolioItem from './PortfolioItem'

test("Portfolio item calculateChange() and latestPrice() works correctly", ()=>{
    const { getByTestId, getByText } = render(<PortfolioItem quantity={1} purchasePrice={100} latestPrice={120} />);

    const changeValue = getByTestId("change-value");

    expect(changeValue).toHaveTextContent("20.00");
})