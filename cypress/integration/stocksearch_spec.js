describe('My First Test', () => {
    it('Wrong login password message', () => {
        cy.visit('/');

        cy.get('form').find('[type="text"]').type('MSFT')
        cy.get('form').find('[type="password"]').type('111')
        cy.get('form').submit()

        cy.get('p').should('contain', 'Wrong')
    })
  })